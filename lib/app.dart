import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'routes/routes.dart';
import 'theme/theme.dart';

class FlutterTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent),
    );

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: FlutterTestThemeData.themeData,
      darkTheme: FlutterTestThemeData.themeData,
      getPages: AppPages.pages,
      defaultTransition: Transition.native,
      initialRoute: Routes.ALERTS,
    );
  }
}
