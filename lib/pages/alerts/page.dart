import 'package:flutter/material.dart';
import 'package:flutter_test_freelancer/theme/theme.dart';
import 'package:get/get.dart';
import 'package:flutter_test_freelancer/common/common.dart';

import 'controller.dart';
import 'widget/widget.dart';

class AlertsPage extends GetView<AlertsController> {
  final _theme = Get.theme;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage('img_background'.assetPathPNG), fit: BoxFit.cover),
        ),
        child: _buildContent(),
      ),
    );
  }

  Widget _buildContent() {
    return Column(
      children: [
        _buildHeader(),
        Expanded(
          child: ListView.separated(
            padding: const EdgeInsets.only(bottom: 20),
            itemBuilder: (_, index) => ItemAlert(
              model: controller.alerts[index],
            ),
            separatorBuilder: (_, index) => 20.verticalSpace,
            itemCount: controller.alerts.length,
          ),
        ),
      ],
    );
  }

  Widget _buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Alerts',
          style: _theme.textTheme.headline4!.text434343,
        ),
        ContainerLeftCross(
          color: AssetColors.colorGrey434343,
          child: Container(
            margin: const EdgeInsets.fromLTRB(35, 10, 20, 10),
            child: Row(
              children: [
                Image.asset(
                  'ic_filter'.assetPathPNG,
                  color: Colors.white,
                  width: 16,
                ),
                4.horizontalSpace,
                Text(
                  'Filter',
                  style: _theme.textTheme.subtitle2!.textWhite,
                ),
              ],
            ),
          ),
        ),
      ],
    ).paddingOnly(left: 30, bottom: 25, top: 60);
  }
}
