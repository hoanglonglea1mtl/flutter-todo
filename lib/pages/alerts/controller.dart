import 'package:flutter_test_freelancer/models/models.dart';
import 'package:get/get.dart';

class AlertsController extends GetxController {
  final alerts = [
    AlertModel(
      id: '1',
      title: 'Pricing Update For 2021',
      content: 'Taking into consideration the positive response from our customers to the '
          'pricing structures during the circuit breaker period, we will be maintaining '
          'the base pricing introduced last year June until further notice',
      isAnnouncement: false,
      createAt: DateTime(2021, 8, 14),
    ),
    AlertModel(
      id: '2',
      title: 'Booking Cancelled',
      content: 'Dear Rishi, we have noted on your request to cancel the '
          'booking dated 20th July (A-180221-1497848). Your refund will be credited '
          'back to your E-Wallet within 3 working days. Feel free to contact us if you '
          'have any questions. Have a great day!',
      isAnnouncement: true,
      createAt: DateTime(2021, 10, 16),
    ),
    AlertModel(
      id: '3',
      title: 'Pricing Update For 2021',
      content: 'Taking into consideration the positive response from our customers to the '
          'pricing structures during the circuit breaker period, we will be maintaining '
          'the base pricing introduced last year June until further notice',
      isAnnouncement: false,
      createAt: DateTime(2021, 11, 25),
    ),
    AlertModel(
      id: '4',
      title: 'Booking Success',
      content: 'Dear Rishi, we have noted on your request to cancel the '
          'booking dated 20th July (A-180221-1497848). Your refund will be credited '
          'back to your E-Wallet within 3 working days. Feel free to contact us if you '
          'have any questions. Have a great day!',
      isAnnouncement: true,
      createAt: DateTime(2021, 12, 6),
    ),
    AlertModel(
      id: '5',
      title: 'Pricing Update For 12/2021',
      content: 'Taking into consideration the positive response from our customers to the '
          'pricing structures during the circuit breaker period, we will be maintaining '
          'the base pricing introduced last year June until further notice',
      isAnnouncement: true,
      createAt: DateTime(2021, 12, 30),
    ),
  ];
}
