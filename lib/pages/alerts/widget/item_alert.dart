import 'package:flutter/material.dart';
import 'package:flutter_test_freelancer/common/common.dart';
import 'package:flutter_test_freelancer/models/models.dart';
import 'package:flutter_test_freelancer/theme/asset_colors.dart';
import 'package:get/get.dart';

class ItemAlert extends StatelessWidget {
  final AlertModel? model;
  final VoidCallback? onTapView;

  ItemAlert({this.model, this.onTapView});

  final _theme = Get.theme;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          padding: const EdgeInsets.only(top: 35),
          height: 210,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _buildContent(),
              _buildButtonView(),
            ],
          ),
        ),
        _buildTitleAndTime(),
      ],
    );
  }

  Widget _buildContent() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.fromLTRB(25, 5, 25, 15),
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (model?.isAnnouncement ?? false)
              Text(
                'Announcement',
                style: _theme.textTheme.overline!.letterSpacing0p1.bold.textColor(AssetColors.colorBlue0D7ABF),
              )
            else
              16.verticalSpace,
            12.verticalSpace,
            Text(
              model?.content ?? '',
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
              style: _theme.textTheme.caption!.heightLine(16),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndTime() {
    return ContainerRightCross(
      color: AssetColors.colorBlue0D7ABF,
      child: Container(
        height: 60,
        margin: const EdgeInsets.only(left: 20, right: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              model?.title ?? '',
              style: _theme.textTheme.headline6!.textWhite,
            ),
            Text(
              (model?.createAt ?? DateTime.now()).formatDateddMMMyyyy,
              style: _theme.textTheme.caption!.textWhite,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButtonView() {
    return InkWell(
      onTap: onTapView,
      child: ContainerLeftCross(
        child: Container(
          margin: const EdgeInsets.fromLTRB(50, 10, 30, 10),
          child: Text(
            'View',
            style: _theme.textTheme.headline6!.textWhite,
          ),
        ),
      ),
    );
  }
}
