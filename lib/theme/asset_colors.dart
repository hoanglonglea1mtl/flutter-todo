import 'package:flutter/material.dart';

class AssetColors {
  static Color shimmerBaseColor = Colors.grey.withOpacity(0.8);
  static Color shimmerHighlightColor = Colors.grey.withOpacity(0.5);
  static const Color textBlack = Color(0xff181A20);
  static const Color primary = Color(0xffFFAC40);
  static const Color colorGreyBFBFBF = Color(0xffBFBFBF);
  static const Color colorGrey333333 = Color(0xff333333);
  static const Color colorGrey262626 = Color(0xff262626);
  static const Color colorGreyB4B2B2 = Color(0xffB4B2B2);
  static const Color colorGrey434343 = Color(0xff434343);
  static const Color colorGrey252525 = Color(0xff252525);
  static const Color colorGrey595959 = Color(0xff595959);
  static const Color colorOrangeE25D20 = Color(0xffE25D20);
  static const Color colorRedEA3D2F = Color(0xffEA3D2F);
  static const Color colorBlue001E62 = Color(0xff001E62);
  static const Color colorBlue0D7ABF = Color(0xff0D7ABF);
  static const Color colorYellowFD9B37 = Color(0xffFD9B37);
}
