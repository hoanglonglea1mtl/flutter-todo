import 'package:flutter/material.dart';
import 'package:flutter_test_freelancer/theme/theme.dart';

class ContainerLeftCross extends StatelessWidget {
  final Color? color;
  final Widget child;

  ContainerLeftCross({this.color, required this.child});

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: LeftCrossCustomPainter(color: color),
      child: child,
    );
  }
}

class LeftCrossCustomPainter extends CustomPainter {
  Color? color;

  LeftCrossCustomPainter({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0 = new Paint()
      ..color = color ?? AssetColors.colorYellowFD9B37
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    Path path_0 = Path();
    path_0.moveTo(size.width, 0);
    path_0.lineTo(size.width, size.height);
    path_0.lineTo(size.width * 0.2575000, size.height);
    path_0.lineTo(0, 0);
    path_0.lineTo(size.width, 0);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
