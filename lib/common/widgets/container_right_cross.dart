import 'package:flutter/material.dart';
import 'package:flutter_test_freelancer/theme/theme.dart';

class ContainerRightCross extends StatelessWidget {
  final Color? color;
  final Widget child;

  ContainerRightCross({this.color, required this.child});

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: RightCrossCustomPainter(color: color),
      child: child,
    );
  }
}

class RightCrossCustomPainter extends CustomPainter {
  Color? color;

  RightCrossCustomPainter({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0 = new Paint()
      ..color = color ?? AssetColors.colorYellowFD9B37
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    Path path_0 = Path();
    path_0.moveTo(size.width, size.height * -0.0085);
    path_0.lineTo(size.width * 0.9000000, size.height);
    path_0.lineTo(size.width * 0.00085, size.height);
    path_0.lineTo(0, 0);
    path_0.lineTo(size.width, size.height * -0.0085);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
