class AlertModel {
  String id;
  String? title;
  DateTime? createAt;
  String? content;
  bool? isAnnouncement;

  AlertModel({required this.id, this.title, this.createAt, this.content, this.isAnnouncement});
}
